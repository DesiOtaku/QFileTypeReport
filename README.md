A simple tool to read the file types of a directory
![Screenshot of QFileTypeReport](https://gitlab.com/DesiOtaku/QFileTypeReport/-/raw/master/docs/screenshot.png?ref_type=heads "Screenshot of QFileTypeReport")
