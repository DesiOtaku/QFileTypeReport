//QFileTypeReport
//Copyright (C) 2024 Dr. Tej A. Shah

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QFileInfo>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QMimeType>
#include <QDebug>
#include <QSettings>
#include <QTime>



MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->actionOpen_Directory,SIGNAL(triggered()),this,SLOT(handleOpenDirCommand()));
    //connect(ui->actionAbout,SIGNAL( triggered() ),this,SLOT( handleAbout() ));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::handleOpenDirCommand()
{
    QSettings settings;
    QString setDef = settings.value("LastDir",QDir::homePath()).toString();
    QString getDirPath = QFileDialog::getExistingDirectory(this,tr("Open Directory"), setDef);
    ui->treeWidget->clear();
    readThroughDir(getDirPath);
    settings.setValue("LastDir",getDirPath);
}

//void MainWindow::handleAbout()
//{

//}

void MainWindow::readThroughDir(QString getDir)
{
    QTime start = QTime::currentTime();

    QDir readDir(getDir);
    quint64 MEGA_BYTE = 1048576;
    QVariantHash results = giveReport(readDir);
    foreach(QString fileType,results.keys()) {
        QStringList topItems = {fileType};
        QTreeWidgetItem *typeWidget = new QTreeWidgetItem(topItems);
        ui->treeWidget->addTopLevelItem(typeWidget);
        QVariantHash fileExts = results[fileType].toHash();
        qulonglong typeSizeCounter =0;
        foreach(QString fileExt, fileExts.keys()) {
            QStringList extItems = {fileExt,QString::number(fileExts[fileExt].toULongLong() / MEGA_BYTE)};
            QTreeWidgetItem *extWidget = new QTreeWidgetItem(extItems);
            typeWidget->addChild(extWidget);
            typeSizeCounter+= fileExts[fileExt].toULongLong();
        }
        typeWidget->setText(1,QString::number(typeSizeCounter / MEGA_BYTE));
    }

    ui->treeWidget->setEnabled(true);

    QTime end =  QTime::currentTime();
    int msecs = start.msecsTo(end);
    ui->statusbar->showMessage(tr("It took %1 milliseconds to read %2").arg(msecs).arg(getDir));
}

QFileInfoList MainWindow::generateFileInfoList(QDir getDir)
{
    QFileInfoList returnMe;
    foreach(QFileInfo item, getDir.entryInfoList(QDir::AllEntries | QDir::NoDotAndDotDot | QDir::NoSymLinks)) {
        if(item.isDir()) {
            QFileInfoList addMe = generateFileInfoList(QDir(item.absoluteFilePath()));
            returnMe.append(addMe);
        }
        else if(item.isFile()) {
            returnMe.append(item);
        }

    }
    return returnMe;
}

//Format:
//   QVariantHash[FileType] -> QVariantHash[fileExt] -> bytesInSize (quint64)


QVariantHash MainWindow::giveReport(QDir getDir)
{
    QFileInfoList fullList = generateFileInfoList(getDir);
    QVariantHash returnMe;
    foreach(QFileInfo item, fullList) {
        QMimeType fileMime = m_mDB.mimeTypeForFile(item);
        QStringList fileTypeStrings = fileMime.name().split("/"); // "filetype/fileExt"; "image/png"
        QString fileType = fileTypeStrings.at(0);
        QString fileExt = fileTypeStrings.at(1);
        QVariantHash fileExtHash;

        if(returnMe.contains(fileType)) { //has that type
            fileExtHash = returnMe[fileType].toHash();
            if(fileExtHash.contains(fileExt)) {
                fileExtHash[fileExt] = fileExtHash[fileExt].toULongLong() + item.size();
            }
            else {
                fileExtHash[fileExt] = item.size();
            }
        }
        else {
            fileExtHash[fileExt] = item.size();
        }
        returnMe[fileType] = fileExtHash;
    }
    return returnMe;
}
